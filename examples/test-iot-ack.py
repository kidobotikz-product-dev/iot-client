from time import sleep
import datetime
from sprw.io import IOT, iotprint


ACCESS_TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImJhYjkxY2U4YjZlYTRiNmI1MjQxZGQyZGFhNmQ4MjQ4NTZlMzJjMTM3Y2RlOGQ2NTlmYmIxMDY1ZWU4MWM2OWZiM2Y4NjNkZTlhMDM4ZGE1In0.eyJhdWQiOiIzIiwianRpIjoiYmFiOTFjZThiNmVhNGI2YjUyNDFkZDJkYWE2ZDgyNDg1NmUzMmMxMzdjZGU4ZDY1OWZiYjEwNjVlZTgxYzY5ZmIzZjg2M2RlOWEwMzhkYTUiLCJpYXQiOjE1Mjc0NTgxMTcsIm5iZiI6MTUyNzQ1ODExNywiZXhwIjoxNTU4OTk0MTE3LCJzdWIiOiIyOTI1Iiwic2NvcGVzIjpbXX0.aWGGvoqCeskScQEFewa6E9Nbtl8A2XJopoOFDazvzF05s-kAjlvZhGeUwFjVK68paZyQWKHawQ_UbTNH1Qs4Gaj7eI4LGJWmfnuGn7FwWQ9lNsZ6efXFIN_qLzxUn7kjorDJtgkuuA9TjDqN39zrqLFVagKG7H4G6M4wSLGzKpPusDZ6JAPkVMgWnEtLLwW481ADR8s8Z1DoWELUusgy-BUeAk8CtYH4xujp9NhE1Qvg-7Rld3OVQBF1I8k6e4PU9AovHYHfrLcGVrhEqvCZItXA_cUVDMF7Aq6emoA1zlyQMmRbcttgqvyV0CWqYLJ_hyuTeA4NEgRE3dOoNC12qC7BiZdUt5i87abfsZgNNRfkSCLzg6fNHWQ8iEJnSI4-ROb3Fe2sBgotEq85yeywWC2k1_xpJDux16RfpheI2mGpWq1_xJPh9c9bU_n7KICxGVrXO42alFs74zjgpxd8jVZoXbiqetxvtFdzxf7UFL9m4EoqUUEX3zqofNF7wPo6a1TXcNX6lk838ujEbJhqJnT6zhpDdKBeBq2C-zK91nAIerjEreMNfdHqmtZPqSyuHozNQeyI8sNpm2HQsUUT0VDkInLe7ENAOkmFmiopmbv76MiCYe2cxBE5XZ5-ioQ-DX7Pksi8ctZma23xphzRcOQDtVsdMKWvY-zu1_ZsHOQ'

print('Starting iot client\n\n')
sp_server = IOT(ACCESS_TOKEN)

previous_state = None

while True:
    before_request_time = datetime.datetime.now()

    current_state = sp_server.get_thing_state_attributes(1)
    after_request_time = datetime.datetime.now()
    print(after_request_time - before_request_time)
    print('State: ' + str(current_state))
    # if previous_state != current_state:
    
    #     status = sp_server.acknowledge_thing_state_attribute(
    #         thing_id=18, value=current_state.value)
    #     print(status)

    previous_state = current_state
    sleep(1)
