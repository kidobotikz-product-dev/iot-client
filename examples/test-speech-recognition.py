from sprw.io import IOT, Assistant, exceptions, iotprint
from signal import pause
from time import sleep
from gpiozero import Button, LED
from signal import pause


ACCESS_TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjY3OGFiZmM5NTQ2NDEyMzVhMTg5MjlkOGVkZGQ1ODU4MGYxMGMyZTE5ODRlZWRlNTQyY2FjODY3YmY4MDk5MWVlNTA1MDQ5Y2VkODQ0Y2Q2In0.eyJhdWQiOiIzIiwianRpIjoiNjc4YWJmYzk1NDY0MTIzNWExODkyOWQ4ZWRkZDU4NTgwZjEwYzJlMTk4NGVlZGU1NDJjYWM4NjdiZjgwOTkxZWU1MDUwNDljZWQ4NDRjZDYiLCJpYXQiOjE1MjQwNDM4NDQsIm5iZiI6MTUyNDA0Mzg0NCwiZXhwIjoxNTU1NTc5ODQ0LCJzdWIiOiIyOTI1Iiwic2NvcGVzIjpbXX0.ggndjpJUpPPApos4ko7qPmtZrzaQnDjQr7MFiA15rGCJNCVQmcbsCBW8TuSI1L4UX6OWyTG1ErmRBIQqz8lNt-rDoiUR5V_9E3KFRDoAKQQvcIdRdZGyEBjcXfXe62_eDLklPuVjpu4MLXT6OKpnNSns38Ka3fZiQ2DFR8rapbbMTAcPrB_qxNtfNW3QRHK7vkdwJEeHHbUkvAnliMAmDvqiX92dW2RIwUnCLQu12kCXYEOcRLVun9JN-cE0DpxYN2sgppK7Nc523t94a8ZQzaViSfcVxVWLAWkRkOwTkI9wTaauwkPVrpMuK6dPDosvC5HJsTbKr0DkHPnGhqgh4zeehJh_4OSVt0MlEEjN7Tu5nuweo5mHe2Bs3qCqCT7N_S6h36PSmMflPVzRTSvN_7Ls_0Qt5V00WTAphvFXhWgIL_PWo8XX_LuluTJxMj0ynXUp3JyXwbQpJ0qnAAmY7-DATPvq5LyluNlqoENiIAFCY6L7nVBiQInryslqutEuZZO00r7mb1QiZ3jYciLhVCCTH9YgkfpU858Z9W6euqVZeN5FLxhZNq6opLruZEzkZNsptAC8cVy2zA6FgMPXF2P5FigHp3SoOCi40UFDRl3uyMTSXrByHUd3wk1UfJoJjFtjFq4aOWWwn-GJ3N5zi1_3YBdxo17-b5pD7oogJBA'

sp_server = IOT(ACCESS_TOKEN)
assistant = Assistant(ACCESS_TOKEN)
assistant.microphone_device_index = 2
button = Button(23)
led1 = LED(22)
led2 = LED(25)
print('Press button 23 to listen for speech')
last_state_is_pressed = False

assistant_thing = sp_server.create_thing('My Assistant', 'OTHERS', 'ASSISTANT')
iotprint(assistant_thing)

while True:

    if button.is_pressed and not last_state_is_pressed:

        try:

            text = assistant.recognize_speech(language='en-IN')
            print(text)
            if 'weather' in text:
                assistant.speak('It is 35 degree Celsius out there')
            if ('alarm' in text) or ('reminder' in text):

                time = assistant.get_datetime_from_text(text)
                reminder_message = 'Default Message'

                if 'remind me to' in text:
                    reminder_message = text.split('remind me to')[1]

                # Example:
                # text = 'Set a reminder at 8 PM. remind me to send a mail to the customer'
                # text.split('remind me to')
                # text will be split as,
                #  [ 0 => 'Set a reminder at 8 PM.',
                #    1 => 'send a mail to the customer' ]

                # Hence, text.split('remind me to')[1] will give the message 'send mail to the customer'

                new_reminder = {'status': 'ACTIVE', 'time': time,
                                'message': reminder_message, 'name': 'Test Reminder'}

                assistant.speak('Alright! Setting the reminder')

                status = sp_server.update_thing_multi_state_attributes(
                    thing_id=assistant_thing.id, reminder=new_reminder)

                print(status)

                assistant.speak('Reminder saved!')

            if ('lights' in text) or ('light' in text) or ('led' in text):
                if 'turn on' in text:
                    assistant.speak('Right away!')
                    led1.on()
                    led2.on()
                    print('LEDs on')

        except exceptions.SpeechRecognitionError as e:
            print(e)

    last_state_is_pressed = button.is_pressed
    sleep(0.1)

pause()
