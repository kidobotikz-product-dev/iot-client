from time import sleep
import datetime
from sprw.io import IOT, iotprint

now = datetime.datetime.now()

ACCESS_TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjcyNDlmMmM4NmQwODZhZmJmNzcxYmNmMGRlZTUwZTYwM2ZkZDg2MTU3YjBkMTBkYzk3YTVlZjkxOWY1M2ZiZTA0NjNjMGU5ZTlhYjliN2Q1In0.eyJhdWQiOiIzIiwianRpIjoiNzI0OWYyYzg2ZDA4NmFmYmY3NzFiY2YwZGVlNTBlNjAzZmRkODYxNTdiMGQxMGRjOTdhNWVmOTE5ZjUzZmJlMDQ2M2MwZTllOWFiOWI3ZDUiLCJpYXQiOjE1MjU2MTU1NzMsIm5iZiI6MTUyNTYxNTU3MywiZXhwIjoxNTU3MTUxNTczLCJzdWIiOiIyOTI1Iiwic2NvcGVzIjpbXX0.ewSATmP3HdMT0D8BpqDaYE7m2O8lT7dVEhuMfEvHzIQaHOGdrAgwufEdS3dZ3WuilMErUFbt0oBDIEXolH38ZNRa5Q-wfw9mNZXRFt6EoFKiW7glwQuNoH45K_KXppzlfIuhoISaThRzmqtL8_W_R6-hqlAmB1ZJBVw4cK5hOLjpjDfYyw_LRNYV37dwxGKUrKjz5AJqdZ4J7sP8CJQ89_sGf_nEv30k5Qc1t6Rfri_zXTUY8llkr2cPLfch6Nx0Tbyf2P_VNMDMLO8eVe947LXEOZWlZDQhpr3RbDZtM7ExM2cpL74_iMa7a4r74o9CaSn5dSMwRXX2ZL37pl6BD3-YpkkpTp6q2GxUKQWNhdEtO8C2qW-V1AN8S1U3f3Ep7-tMoQSHnRplKvrw9FKkEa81lNcKp4sTdea3xXhBNBmvp6HAsvQ7HSrbldroEDOXgJ1HBVPA_VSt257JKO_Lhnp9Ee5sNUbeecNgwZi4PlGPJZnbIcO0L6r1XAuhPi29tqwlmiV66Dux2pj6GpCMmE1ukfuXXYzodFtFkQqqeKSiWF-Htjzh-N4XOyNzClX0ufdgLy2L8urTuCCDExzWur6GqUuRMxrr0XiIWparXcmayS1H8SPq2XQRhg_x0qRVk9jTGQWPxqU6sRnThybkPTzjv-U2E3i_32bo5rnKJxo'

print('Starting iot client\n\n')
sp_server = IOT(ACCESS_TOKEN)
things = sp_server.get_things()
iotprint(things)
print('=============')

d_input = sp_server.create_thing(
    'My digital input', 'INPUT_DEVICE', 'DIGITAL_IP', pin_number=2)
iotprint(d_input)


thing_state = sp_server.get_thing_state_attributes(thing_id=d_input.id)
print(thing_state)

a_input = sp_server.create_thing(
    'My analog input', 'INPUT_DEVICE', 'ANALOG_IP', pin_number=2)
iotprint(a_input)


thing_state = sp_server.get_thing_state_attributes(thing_id=a_input.id)
print(thing_state)

button = sp_server.create_thing('My Button 2', 'INPUT_DEVICE', 'BUTTON', pin_number = 2)
iotprint(button)

# thing_state = sp_server.get_thing_state_attributes(thing_id=button.id)
# print(thing_state)

# sensor = sp_server.create_thing('Proximity sensor', 'INPUT_DEVICE', 'PROXIMITY', pin_number=2)
# iotprint(sensor)

# thing_state = sp_server.get_thing_state_attributes(thing_id=sensor.id)
# print(thing_state)

# sensor = sp_server.create_thing(
#     'light sensor', 'INPUT_DEVICE', 'LIGHT', pin_number=2)
# iotprint(sensor)

# thing_state = sp_server.get_thing_state_attributes(thing_id=sensor.id)
# print(thing_state)

# sensor = sp_server.create_thing(
#     'Temp sensor', 'INPUT_DEVICE', 'TEMPERATURE', pin_number=2)
# iotprint(sensor)

# thing_state = sp_server.get_thing_state_attributes(thing_id=sensor.id)
# print(thing_state)

thing = sp_server.create_thing('My OP Device', 'OUTPUT_DEVICE', 'PWM_OP')
iotprint(thing)


left_motor = sp_server.create_thing('Left Motor', 'OUTPUT_DEVICE', 'MOTOR', pin_number = 2)
iotprint ("left_motor: ")
iotprint (left_motor)

motor = sp_server.create_thing('My Motor', 'OUTPUT_DEVICE', 'MOTOR', pin_number = 3)


iotprint ("right_motor: ")
iotprint(motor)


led = sp_server.create_thing('LED 1', 'OUTPUT_DEVICE', 'LED', pin_number = 3, color='Red')
iotprint("led: ")
iotprint(led)

rgb_led = sp_server.create_thing('RGB Led', 'OUTPUT_DEVICE', 'RGB_LED', pin_number = 3, color='Red')
iotprint("rgb_led: ")
iotprint(rgb_led)

d_op = sp_server.create_thing(
    'Digital op device', 'OUTPUT_DEVICE', 'DIGITAL_OP', pin_number=3, color='Red')
iotprint("d_op: ")
iotprint(d_op)


# # while True:

# status = sp_server.update_thing_state_attributes(
#         thing_id=sensor.id, value=5)
# print(status)

# # status = sp_server.update_thing_state_attributes(
# #     thing_id=led.id, value=0.5)
# # print(status)

# status = sp_server.update_thing_state_attributes(
#     thing_id=d_op.id, value=1)
# print(status)

# status = sp_server.update_thing_state_attributes(
#     thing_id=rgb_led.id, value_r=0.7)
# print(status)

# status = sp_server.update_thing_state_attributes(
#     thing_id=left_motor.id, direction='FORWARD')
# print(status)

# sleep(1)

# thing_state = sp_server.get_thing_state_attributes(thing_id=led.id)
# print(thing_state)

# status = sp_server.acknowledge_thing_state_attribute(
#     thing_id=led.id, value=thing_state.value)
# print(status)

# led_custom_attributes = sp_server.get_thing_custom_attributes(thing_id=led.id)
# print(led_custom_attributes)

# status = sp_server.update_thing_state_attributes(
#     thing_id=motor.id, direction='FORWARD')

# print("Update thing custom attr status: " + str(status))

# color_led = sp_server.create_thing('Color LED 2', 'OUTPUT_DEVICE', 'RGB_LED', pin_number = 5)
# iotprint("color_led: ")
# iotprint(color_led)

# status = sp_server.update_thing_custom_attributes(thing_id = motor.id, rpm = 150)
# print ("Update thing custom attr status: " + str(status))

# status = sp_server.delete_thing(thing_id=motor.id)
# print(status)




