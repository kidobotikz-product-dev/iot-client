import datetime
from time import sleep
from sprw.io import IOT, iotprint, Assistant

ACCESS_TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijg5NGRhMzBkYTk5NTg4NmE4ZWE2Y2E0YTdkN2RmYzhkMThjMzc3MzY0ZjljYzJiMTgwZDk3NDMzNTVjZWFkN2UwZWVlZmU2ODkxNGEwYTVjIn0.eyJhdWQiOiIxIiwianRpIjoiODk0ZGEzMGRhOTk1ODg2YThlYTZjYTRhN2Q3ZGZjOGQxOGMzNzczNjRmOWNjMmIxODBkOTc0MzM1NWNlYWQ3ZTBlZWVmZTY4OTE0YTBhNWMiLCJpYXQiOjE1MTg5NjQxMTYsIm5iZiI6MTUxODk2NDExNiwiZXhwIjoxNTUwNTAwMTE1LCJzdWIiOiIyOTI1Iiwic2NvcGVzIjpbXX0.CLQx5-AjcBUfY-XA85A41XuYsM-nrnhq5T7REoaV9YoN4hn0aTTlSURmwPyizvAyu_cWRZjnRSalPjvFhnRX3iZqnbFShADv-kc0_sYyEzdxbUQABPSDH-jK2tVXwylHYFwMeUusk2GwvxPsklsN1ECH_6Mo74TvSS5rMHjjul8NClL6une3OimSLl2oB8ZUPaU4l6-Fi_VgnAUEQAgL2LOFtgIW_6LFdJg2cltU-__efyqJUdkE776EV4iqN-H5y_TP3IVKOw-LkvYTTrTk_SrLimDrQfyy_G16D6CvDcyl4EGKNSr1dwpswTwaRz3O4KF8bBqQorCOLSLrtM_Pt8p8owdzXvFI09zxE2vZ67Inevf6H2VCTddXZte1fGM16GXsftliUjvFZKzjZUFY47sVXgBHIvh63uD4ML4_Z8xHPF5Gob9SLzitp5jD1Xs75FkswMTMmrl9-PZtlnGGG7DJJDMlgCJfRT6IgPhN94ATlqd_QVJXNYAUMgYG6nn5gLDRCcPNeCKxkQlTffxzFfPVmQ4kH-VEXjDyBVDTtilkm9q1UQVBZG3tLoyFZrKdetT-Gwy-OVgRKbbrxOIZgvA8CgzLu4_KD7OIPklNfWcly4z1jaWBKk8sF0x_f39F3BLX9oCymGgnTB8rJT4ATJKGsOW6lQL9Wo6ePaRe3pk'

sp_server = IOT(ACCESS_TOKEN)
assistant = Assistant(ACCESS_TOKEN)

iotprint(assistant.get_voices())

now = assistant.get_current_datetime()
print(now)
updated_time = now + assistant.datetime_offset(hours=2)
print(updated_time)

time_in_words = assistant.get_time_in_words(now.hour, now.minute)
print(time_in_words)

month = assistant.get_month_in_words(now.month)
print(month)

messages = [
    "Hello", "Hi", "Test", "Testing", "Good", "Bye"
]

random_message = assistant.get_random_message(messages)
print(random_message)



last_reminders_fetch_time = None
reminders = None


assistant_thing = sp_server.create_thing('My Assistant', 'OTHERS', 'ASSISTANT')
iotprint(assistant_thing)

##Create new reminder
# new_reminder = {'status': 'ACTIVE', 'time': now,
#                 'message': 'Hello', 'name': 'Python'}

# status = sp_server.update_thing_multi_state_attributes(
#     thing_id=assistant_thing.id, reminder=new_reminder)

# print(status)
while True:

    now = assistant.get_current_datetime()

    if last_reminders_fetch_time != None:
        time_difference = now - last_reminders_fetch_time

    if (last_reminders_fetch_time == None) or time_difference > assistant.datetime_offset(seconds=10):
        
        last_reminders_fetch_time = now
        
        reminders_required_before_time = now + assistant.datetime_offset(hours=1)

        print('reminders_required_before_time')
        print(reminders_required_before_time)


        # Create condition based on which reminders should be fetched
        reminder_condition = {
            'time': reminders_required_before_time,
            'status' : 'ACTIVE'
        }

        multi_state_attributes = sp_server.get_thing_multi_state_attributes(
            thing_id=assistant_thing.id, reminder_condition=reminder_condition)

        reminders = multi_state_attributes.reminders

        iotprint("reminders: ")
        iotprint(reminders)

    # Iterate through the created reminders and speak the message using the assistant when the time matches

    for current_reminder in reminders:

        reminder_time = current_reminder.time
        
        now = assistant.get_current_datetime()
        print(now)
        print(reminder_time)
        if now >= reminder_time and current_reminder.status == 'ACTIVE':
            
            assistant.speak(current_reminder.message)
            current_reminder.status = 'DISMISSED'
            status = sp_server.update_thing_multi_state_attributes(
                thing_id=assistant_thing.id, reminder=current_reminder)
            print(status)


##Update an existing reminder using reminder_id
# current_reminder = {'id': 8, 'status': 'ACTIVE'}
# status = sp_server.update_thing_multi_state_attributes(
#     thing_id=assistant_thing.id, reminder=current_reminder)

##Create new reminder
# new_reminder = {'status': 'ACTIVE', 'time': str(now), 'message' : 'Hello', 'name' : 'Python'}

# status = sp_server.update_thing_multi_state_attributes(
#     thing_id=assistant_thing.id, reminder=new_reminder)
 
# iotprint(status)
