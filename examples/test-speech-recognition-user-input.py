from sprw.io import IOT, Assistant, iotprint, exceptions
from signal import pause
from time import sleep
from signal import pause


ACCESS_TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU0NDYzYmJlMDlkNjM2Y2NiMDc0YTJmMTFhZTUzMGZlYzI5NTEyY2MyODFiMWI2ZWNmNzkzMjRlNGE0OTQ1YzA4Yjg2ZjBmN2YxMDRkZmEyIn0.eyJhdWQiOiIzIiwianRpIjoiNTQ0NjNiYmUwOWQ2MzZjY2IwNzRhMmYxMWFlNTMwZmVjMjk1MTJjYzI4MWIxYjZlY2Y3OTMyNGU0YTQ5NDVjMDhiODZmMGY3ZjEwNGRmYTIiLCJpYXQiOjE1MjUyNjcxMzcsIm5iZiI6MTUyNTI2NzEzNywiZXhwIjoxNTU2ODAzMTM3LCJzdWIiOiIyOTI1Iiwic2NvcGVzIjpbXX0.XUzFJLI89kUxS5PvnWJcQGENf61Q4JDD21yoPXGqB4cvWdh7wcL2W9PS3Cl5PjMHqJ5fZ70H9AuNZ7OaNPrwM2Q8j3GV0P9WgwErQmPjHrrzIt4pbG4Ec9o7kYZBu_yk9CjcGNPV_oq_XU26g-FWpG3OSnObxxLQ6VMpvSgm9LUoTpg_n0lLdBcbkembBfTmdkrIli1EnglIwk_xg3Mu2fAAleAC59wzn88FTqgXz5540ncfMYhR6bUHm-HSn1ApAqC4CgN0y19qJLY2p-VvaFQGD3nJG8AVlW2fKxH8zhVVcX3YBmFMXGLVSNaE8lJl5HTQzMFd6o83JLeTopN3AKg6FWyrEP_pFmDmwnSBAqVu7VNQIdY6qA8HTWjFzezSNJLpSIZ89tjBJo8PyGlX_d309STziavZVtit_ic1uiNzA6raYn7Fek3NJZu9sbBw9VscE7gfZRQIuLcvt-2f3Uulj6K17TBNPOKcG0-Up2xaQWV9C87BgQEjKyEOyXULhWtF6z6rN6tI4_HcE25vmeJftGRnkEv8VcDmDDVKYfi-89AIdYUs68FQrXIxzyyC5H4leaDReZDFRskwpDyHFfQJeklXjWRecoUAujJfT8SrJwneGXS79wuUWpRhI-meQYGGYmf58jE87EB_NAHBtnXm1Yo-e79vHU7i5CH_Ky4'

sp_server = IOT(ACCESS_TOKEN)
assistant = Assistant(ACCESS_TOKEN)
assistant_thing = sp_server.create_thing('My Assistant', 'OTHERS', 'ASSISTANT')
iotprint(assistant_thing)

# assistant.microphone_device_index = 1
while True:
    user_input = input("Waiting for input..")
    print(user_input)

    if (user_input == '1'):

        try:

            text = assistant.recognize_speech(language='ta-IN')
            print(text)
            
            if ('alarm' in text) or ('reminder' in text):
                
                assistant.speak('Alright! Setting the reminder')
                time = assistant.get_datetime_from_text(text)
                reminder_message = 'Default Message'
                
                if 'remind me to' in text:
                    reminder_message = text.split('remind me to')[1]
                
                # Example:
                # text = 'Set a reminder at 8 PM. remind me to send a mail to the customer'
                # text.split('remind me to')
                # text will be split as,
                #  [ 0 => 'Set a reminder at 8 PM.',
                #    1 => 'send a mail to the customer' ]

                # Hence, text.split('remind me to')[1] will give the message 'send mail to the customer'

                new_reminder = {'status': 'ACTIVE', 'time': time,
                                'message': reminder_message, 'name': 'Test Reminder'}
                
                status = sp_server.update_thing_multi_state_attributes(
                    thing_id=assistant_thing.id, reminder=new_reminder)
                
                print (status)

                assistant.speak('Reminder saved')

        except exceptions.SpeechRecognitionError as e:
            print(e)
