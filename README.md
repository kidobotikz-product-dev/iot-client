# README #


### What is this repository for? ###

* Library which allows developers to write Python script to use their devices to access the SPRW IoT Platform.
* 1.0

### How do I get set up? ###

#### Installation

##### Minimum Requirements

    * Requires Python 3.5+

##### Installing using pip

Use ``pip`` to install the package::

    pip3 install sprw-iot

### Documentation ###

(http://sprw-iot.readthedocs.io/en/latest/)

### Who do I talk to? ###

* developer@sproboticworks.com