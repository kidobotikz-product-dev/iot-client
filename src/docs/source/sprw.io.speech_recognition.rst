sprw\.io\.speech\_recognition package
=====================================

Module contents
---------------

.. automodule:: sprw.io.speech_recognition
    :members:
    :undoc-members:
    :show-inheritance:
