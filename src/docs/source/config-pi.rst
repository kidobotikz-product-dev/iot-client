Configuring Raspberry Pi
========================

Enabling Hardware SPI Communication to use MCP3008 ADC
------------------------------------------------------

This should be done in order to enable Hardware SPI to use MCP3008 ADC present in the SPRW IoT Hardware, so that analog inputs can be read.
The default Raspbian image disables SPI by default so before you can use it the interface must be enabled. 

1. After your Pi boots to the desktop, go to **Menu > Preferences > Raspberry Pi Configuration**

.. image:: _static/raspiconfig_gui_01.png

2. Then you simply need to select the “Interfaces” tab and set SPI to “Enabled” :

.. image:: _static/raspiconfig_gui_02.png

3. When prompted select “Yes” to reboot so that the changes take effect.


Audio Configuration
---------------------------

The default audio output will be through either HDMI or the 3.5 mm audio jack.

The 3.5 mm audio jack serves only for output devices (speakers) and hence microphone cannot be connected directly to it. For this purpose, USB Audio adapter is used.

1. Connect the speaker and microphone to the USB Audio adapter and insert the adapter to one of the USB ports.

2. Go to **Menu > Preferences > Audio Device Settings**

.. image:: _static/raspi-usb-audio-1.png

3. From the sound card dropdown, select **USB Audio Device (Alsa Mixer)**

4. Now click on **Make Default** button at the bottom to make this sound card as the default one.

.. image:: _static/raspi-usb-audio-2.png

5. Reboot your pi for the changes to take effect.

6. In order to adjust the volume of the speaker and the microphone, click on **Select Controls** and choose the required modules whose volume is to be adjusted.
