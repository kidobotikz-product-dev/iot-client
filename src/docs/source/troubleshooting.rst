Troubleshooting
============

Unauthenticated to access the SPRW server - SPRWIoTExceptions.Error: {'message': 'Unauthenticated.'}
----------------------------------------------------------------------------------------------------

1. This denotes that the **ACCESS_TOKEN** used to initialise the SPRW **IOT** and **Assistant** from sprw.io header is invalid (or) expired.

2. Regenerate the token from the **Developer Settings** in IOT Portal.

3. Copy and paste the newly generated token in place of your old **ACCESS_TOKEN** used in the python program. 

