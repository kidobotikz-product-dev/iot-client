sprw package
============

Subpackages
-----------

.. toctree::

    sprw.io

Module contents
---------------

.. automodule:: sprw
    :members:
    :undoc-members:
    :show-inheritance:
