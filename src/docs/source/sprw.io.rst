sprw\.io package
================

Subpackages
-----------

.. toctree::

    sprw.io.exceptions
    sprw.io.speech_recognition

Submodules
----------

sprw\.io\.assistant module
--------------------------

.. automodule:: sprw.io.assistant
    :members:
    :undoc-members:
    :show-inheritance:

sprw\.io\.config module
-----------------------

.. automodule:: sprw.io.config
    :members:
    :undoc-members:
    :show-inheritance:

sprw\.io\.iot module
--------------------

.. automodule:: sprw.io.iot
    :members:
    :undoc-members:
    :show-inheritance:

sprw\.io\.print module
----------------------

.. automodule:: sprw.io.print
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: sprw.io
    :members:
    :undoc-members:
    :show-inheritance:
