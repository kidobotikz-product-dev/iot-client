Installation
============

Minimum Requirements
---------------------------

    * Requires Python 3.5+

Installing using pip
---------------------------

If you have python 2 as the default and having python 3 seperately, use ``pip3`` (Python 3's package management system) to install the package::

    sudo pip3 install sprw-iot

If you have python 3 as the default, use ``pip`` to install the package::

    sudo pip install sprw-iot

Upgrading using pip
---------------------------

Use the following command to upgrade the ``sprw-iot`` library to the latest version,

If you have python 2 as the default and having python 3 seperately, use ``pip3`` (Python 3's package management system)::
        
    sudo pip3 install --upgrade --force-reinstall --no-cache-dir sprw-iot

Default version Python 3::
        
    sudo pip install --upgrade --force-reinstall --no-cache-dir sprw-iot


Installing required packages for Speech Recognition
-----------------------------------------------------------

Execute the following commands in terminal,

1. Update package respostiory::

    sudo apt-get update

2. Install FLAC (Free Lossless Audio Codec)::

    sudo apt-get install flac

3. Install portaudio library development package (portaudio19-dev), the python development package (python-all-dev) and pyAudio::

    sudo apt-get install portaudio19-dev python3-all-dev && sudo pip3 install pyaudio

4. Install Google API Python Client::

    sudo pip3 install --upgrade google-api-python-client oauth2client
