.. SPRW IoT documentation master file, created by
   sphinx-quickstart on Mon Jan  1 17:51:00 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SPRW IoT's documentation!
====================================
A simple library which allows developers to write Python script to use their devices to access the SPRW IoT Platform.


.. toctree::
    :numbered:
    :caption: Contents:

    install
    config-pi
    api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
