from setuptools import setup, find_packages

setup(
    name="sprw-iot",
    version="1.7",
    packages=find_packages(),
    install_requires=[
        'boto3',
        'pygame==1.9.3',
        'easydict',
        'requests',
        'parsedatetime',
        'pytz',
        'requests-futures'
    ],
    python_requires='>=3',
    description = 'Python Library for SPRW IoT',
    long_description= 'A Python Library for communicating with SPRW IoT Server.',
    author = 'Developer SPRW',
    author_email = 'developer@sproboticworks.com',
    keywords = ['sprw', 'iot', 'io', 'client', 'sprw-iot'],
)
